package de.fuchspfoten.harald;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.Event;

import java.util.function.Consumer;

/**
 * Represents a registration.
 */
@RequiredArgsConstructor
public class EditingWandRegistration {

    /**
     * The consumer for the registration.
     */
    private @Getter final Consumer<Event> consumer;

    /**
     * The description text key for the registration.
     */
    private @Getter final String descriptionKey;
}
