package de.fuchspfoten.harald;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.itemtool.ItemStorage;
import lombok.Getter;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * The main plugin class.
 */
public class HaraldPlugin extends JavaPlugin implements Listener {

    /**
     * The singleton plugin instance.
     */
    private @Getter static HaraldPlugin self;

    /**
     * A matcher for the editing wand.
     */
    private ItemMatcher wandMatcher;

    /**
     * Maps uuids to wand offsets.
     */
    private final Map<UUID, Integer> playerWandOffsets = new HashMap<>();

    @Override
    public void onEnable() {
        self = this;

        final ItemStorage storage = Bukkit.getServicesManager().load(ItemStorage.class);
        wandMatcher = new ItemMatcher(storage.load("tools.editing_wand"));

        // Register messages.
        // ...

        // Register command executor modules.
        // ...

        // Register modules.
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerItemHeld(final PlayerItemHeldEvent event) {
        if (!event.getPlayer().isSneaking()) {
            return;
        }

        final ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        wandMatcher.reset(itemInHand);
        if (!wandMatcher.isMatch() || !event.getPlayer().hasPermission("harald.wand")) {
            return;
        }

        if (EditingWand.getNumRegistrations() == 0) {
            return;
        }

        event.setCancelled(true);
        final int nextSlot = Math.floorMod(event.getPreviousSlot() + 1, 9);
        final int prevSlot = Math.floorMod(event.getPreviousSlot() - 1, 9);
        playerWandOffsets.putIfAbsent(event.getPlayer().getUniqueId(), 0);
        if (nextSlot == event.getNewSlot()) {
            playerWandOffsets.computeIfPresent(event.getPlayer().getUniqueId(),
                    (u, i) -> Math.floorMod(i + 1, EditingWand.getNumRegistrations()));
        } else if (prevSlot == event.getNewSlot()) {
            playerWandOffsets.computeIfPresent(event.getPlayer().getUniqueId(),
                    (u, i) -> Math.floorMod(i - 1, EditingWand.getNumRegistrations()));
        }

        final int offset = playerWandOffsets.get(event.getPlayer().getUniqueId());
        final EditingWandRegistration handler = EditingWand.getRegistration(offset);
        if (handler != null) {
            final String desc = Messenger.getFormat(handler.getDescriptionKey());
            final String fullBarText = String.format("[%1$d] %2$s", offset, desc);
            event.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                    TextComponent.fromLegacyText(fullBarText));
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(final PlayerInteractEntityEvent event) {
        final ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        wandMatcher.reset(itemInHand);
        if (!wandMatcher.isMatch() || !event.getPlayer().hasPermission("harald.wand")) {
            return;
        }

        event.setCancelled(true);
        final int offset = playerWandOffsets.getOrDefault(event.getPlayer().getUniqueId(), 0);
        final EditingWandRegistration handler = EditingWand.getRegistration(offset);
        if (handler != null) {
            handler.getConsumer().accept(event);
        }
    }
}
