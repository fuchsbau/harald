package de.fuchspfoten.harald;

import java.util.ArrayList;
import java.util.List;

/**
 * API class to interact with the editing wand from other plugins.
 */
public final class EditingWand {

    /**
     * The hooks for using the wand.
     */
    private static final List<EditingWandRegistration> hooks = new ArrayList<>();

    /**
     * Fetches the registration at the given offset.
     *
     * @param offset The offset at which the registration resides.
     * @return The registration.
     */
    public static EditingWandRegistration getRegistration(final int offset) {
        if (hooks.size() > offset) {
            return hooks.get(offset);
        }
        return null;
    }

    /**
     * Returns the number of registrations.
     *
     * @return The number of registrations.
     */
    public static int getNumRegistrations() {
        return hooks.size();
    }

    /**
     * Registers the given registration.
     *
     * @param registration The registration.
     */
    public static void register(final EditingWandRegistration registration) {
        hooks.add(registration);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private EditingWand() {
    }
}
